'use strict'

const express=require('express');
const userController=require('../controllers/user');
const authController=require('../controllers/auth');
const movimientosController=require('../controllers/movimientos');
const seg=require('../middlewares');
const api = express.Router();

api.get('/usuario/:id/cuentas',seg.isAuth,userController.getUsuarioCuentas);
api.get('/usuario/cuentas/:nroCuenta',userController.getUsuarioByCuenta);
api.get('/usuario/:idUsuario/cuentas/:idCuenta/movimientos',seg.isAuth,userController.getUsuarioMovimientosByCuenta);

api.post('/movimientos',seg.isAuth,movimientosController.saveMovimiento);

//LOGIN
api.post('/login',authController.login);
api.post('/logout',authController.logout);

//TOKEN
api.get('/seg',seg.isAuth,function (req,res){
  res.status(200).send({message:'ACCESO OK'});
});
api.get('/',function (req,res){
  res.status(200).send({message:'API TECHU'});
});

module.exports=api;
