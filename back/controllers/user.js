'use strict'

var requestJson = require('request-json');
const config=require('../config');
const url=config.mlab_host+config.mlab_db+'collections/';
var generate_id=0;


/*doCuentasGetByUser*/
//GET ALL ACCOUNT USER BY ID
function getUsuarioCuentas(request,response){
    var client = requestJson.createClient(url);
    console.info('id q llega: '+request.params.id);
    const queryName='q={"idUsuario":'+request.params.id+'}&f={"_id":0}&';
    client.get(config.mlab_collection_cuenta+'?'+queryName+config.mlab_key, function(err, res, body) {
    var respuesta=body;
    console.info(JSON.stringify(respuesta));
    response.send(respuesta);
  });
}

//movimientosByUserAJAXResponse
//GET MOVIEENTS USER BY USER AND CUENTA
function getUsuarioMovimientosByCuenta(request,response){
  console.info('Entro a getUsuarioMovimientosByCuenta');
   var client = requestJson.createClient(url);
   //Obteniendo cuentas de usuario
   const queryName='q={"idUsuario":'+request.params.idUsuario+', "idCuenta":'+request.params.idCuenta+'}&f={"_id":0}&';
   console.info("usuario que llega : "+request.params.idUsuario);
   console.info("cuenta que llega : "+request.params.idCuenta);
   client.get(config.mlab_collection_movimiento+'?'+queryName+config.mlab_key, function(err, res, body) {
     var respuestaMovimientos=body;
     console.info(JSON.stringify(body));
     response.send(respuestaMovimientos);
   });
}

//GET USER BY ACCOUNT
function getUsuarioByCuenta(request,response){

   console.info('Entro a busqueda de usuario por cuenta: '+request.params.nroCuenta);
   var client = requestJson.createClient(url);
   //Obteniendo cuentas de usuario
   //const queryName='q={"cuentas.idCuenta":'+request.params.idCuenta+'}&f={"cuentas":0, "_id":0}&';
   //Coloco cuentas.movimientos 0 para que obtenga las cuentas, pero no el array de movimientos 
   const queryName='q={"nroCuenta":"'+request.params.nroCuenta+'"}&f={"_id":0}&';
   client.get(config.mlab_collection_cuenta+'?'+queryName+config.mlab_key, function(err, res, body) {
    var respuesta=body[0];

    if (respuesta==undefined) {response.status(400).send("Cuenta inexistente");return;}
    var idUsuario = respuesta.idUsuario;
    var idCuenta =respuesta.idCuenta;

    const queryNameUsuario='q={"idUsuario":'+idUsuario+'}&f={"_id":0}&';
    client.get(config.mlab_collection_usuario+'?'+queryNameUsuario+config.mlab_key, function(err, res, body) {
      var respuesta=body[0];
    
      var nombres = respuesta.nombres;
      var apellidos =respuesta.apellidos;
      var usuarioEncontrado={"idUsuario":idUsuario,"idCuenta":idCuenta,"nombres":nombres,"apellidos":apellidos};
      response.send(usuarioEncontrado);
    });

   });
}

module.exports={
  getUsuarioCuentas,
  getUsuarioByCuenta,
  getUsuarioMovimientosByCuenta,
};
