'use strict'

var requestJson = require('request-json');
const config=require('../config');
const url=config.mlab_host+config.mlab_db+'collections/';
var generate_id_mov=0;
//GET ALL MOVIEENTS
function saveMovimiento(request,response){
    //Obtener cantidad de movimientos
    var client = requestJson.createClient(url);
    const queryName='f={"_id":0}&';
    
    var datetime = new Date();
    var fechaActual = datetime.toISOString().slice(0,10);
    console.info(fechaActual);
    //1. Grabo movimiento de cuenta de cargo

    client.get(config.mlab_collection_movimiento+'?'+queryName+config.mlab_key, function(err, res, body) {
        generate_id_mov=body.length;
        var data = {
          "idUsuario" : request.body.idUsuarioCargo,
          "idCuenta" : request.body.idCuentaCargo,
          "idMovimiento" : generate_id_mov+1,
          "cuentaRelacionada" : request.body.cuentaAbono,
          "tipoMovimiento" : "cargo",
          "monto" : request.body.montoTransferir,
          "referencia" : request.body.referencia,
          "fecha":fechaActual
        };

        client.post(config.mlab_collection_movimiento+'?'+config.mlab_key, data, function(err, res, body) {
            if(err)console.log(err);
            //2. Grabo movimiento de cuenta de abono
            var data = {
              "idUsuario" : request.body.idUsuarioAbono,
              "idCuenta" : request.body.idCuentaAbono,
              "idMovimiento" : generate_id_mov+1,
              "cuentaRelacionada" : request.body.cuentaCargo,
              "tipoMovimiento" : "abono",
              "monto" : request.body.montoTransferir,
              "referencia" : request.body.referencia,
              "fecha":fechaActual
            };
            var client = requestJson.createClient(url);
            client.post(config.mlab_collection_movimiento+'?'+config.mlab_key, data, function(err, res, body) {

                response.send(body);

            });
        });
    })
      


    console.info(JSON.stringify(request.body));
}

module.exports={
  saveMovimiento,
};
